#include "graph.hpp"

using namespace std;
using namespace graph;

void Graph::link(int v, int u) {
	if (data.find(v) == data.end()) {
		data[v] = new vector<int>();
	}
	data[v]->push_back(u);
	if (data.find(u) == data.end()) {
		data[u] = new vector<int>();
	}
	data[u]->push_back(v);
}	

int Graph::eccentricity(int v) {
	//vector<int> seen;
	unordered_map<int, int> seen;
	queue<int> current;
	queue<int> next;

	int level = 0;
	next.push(v);

	while (!next.empty()) {
		swap(current, next);
		while (!current.empty()) {
			v = current.front();
			current.pop();
			
			if (seen.find(v) != seen.end()) {
				//cout << level << ": " << v << " already seen" << endl;
				continue;
			}

			vector<int> &neighbohood_of_v = *(data[v]);
			for (auto u: neighbohood_of_v) {
				next.push(u);
			}
			seen[v] = v;
		}
		level += 1;
	}
	return level;
}

int Graph::diameter() {
	int max_e = 0;
	for (auto item: data) {
		int e_v = eccentricity(item.first);
		max_e = e_v > max_e ? e_v : max_e;
	}
	return max_e;
}

std::ostream& operator<<(std::ostream& os, const Graph& obj) {
	for (auto item: obj.getData()) {
		os << item.first << ": ";
		for (auto u: *(item.second)) {
			os << u << " ";
		}
		os << endl;
	}
	return os;
}