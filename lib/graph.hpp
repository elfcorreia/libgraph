#ifndef GRAPH_H
#define GRAPH_H

#include <ostream>
#include <unordered_map>
#include <vector>
#include <queue>
#include <algorithm>

namespace graph {	

	class Graph {
	private:
		std::unordered_map<int, std::vector<int>*> data;
	public:
		Graph() {}	
		~Graph() {
			//TODO: Implementar
		}

		std::unordered_map<int, std::vector<int>*> getData() const { return data; }

		std::vector<int> getVertices() const;
		std::vector<int> getNeibohoods() const;
		
		void vertex(int v);
		void link(int v, int u);

		std::vector<int> getVertices();		

		int eccentricity(int v);

		int diameter();	

	};

	void toDot(std::ostream &os, Graph &g);
}

std::ostream& operator<<(std::ostream& os, const graph::Graph& obj);

#endif