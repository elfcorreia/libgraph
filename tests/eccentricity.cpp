#include <iostream>
#include "graph"
#include <catch2/catch.hpp>

using namespace std;
using namespace graph;

int test1() {
	Graph g;
	
	g.link(0, 1);
	g.link(1, 2);
	g.link(2, 3);
	g.link(3, 4);
	g.link(2, 7);
	g.link(3, 7);
	g.link(3, 5);
	g.link(7, 5);
	g.link(5, 6);
	g.link(0, 5);

	cout << "eccentricity " << g.eccentricity(0) << endl;
	cout << "diameter " << g.diameter() << endl;
	return 1;
}

TEST_CASE("Basic creation", "[creation]") {
	//REQUIRE(creation("1CEM.pdb"));
	REQUIRE(test1())
}