#include "iobio.hpp"
#include <fstream>
#include <catch2/catch.hpp>

using namespace std;
using namespace iobio;

bool echo(string filename) {
	try {
		PDBReader f(filename);
		while (f.next()) {
			auto r = f.getRecord();			
			cout << "line_idx=" << f.getLineIdx() << " type=" << r.type << " line=" << f.getLine();
		}
	} catch (fstream::failure &e) {
		cout << "Exception opening " << filename << e << endl;
	}
}

bool creation(string filename) {
	ifstream file(filename);
	PDBFile pdb(&file);	
	return true;
}

TEST_CASE("Basic creation", "[creation]") {
	//REQUIRE(creation("1CEM.pdb"));
	REQUIRE(echo("1CEM.pdb"))
}